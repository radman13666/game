# GAME

By William Kibirango

## DESCRIPTION

This is a CLI game that was inspired by many cartoon shows and video games I love: Bakugan Battle Brawlers, Digata Defenders, Mortal Kombat and Chaotic.

The game can be downloaded from [here](https://bitbucket.org/radman13666/game/downloads/game-v1.1.jar).

## HOW TO RUN

Assuming you have Java installed on your machine, running this with a Bash shell,

```text
$ java -jar game.jar
Usage: game <mode> <numOfBattles> <opponent> <username(s)>
	        mode --- v (versus mode) | s (story mode)
	numOfBattles --- number of battles to fight with opponent (0 < numOfBattles <= 105)
	    opponent --- c (computer) | h (human player)
	 username(s) --- 1 username if opponent=c | 2 usernames if opponent=h (1st username is Challenger)

Usage Example #1: game v 3 c rad
	Example #1 means => play a versus-mode game of 3 battles with 'rad' user as challenger and the computer as the defender.

Usage Example #2: game s 4 h rad ole
	Example #2 means => play a story-mode game of 4 battles with 'rad' user as challenger and 'ole' user as defender.
```

Fill in the arguments as indicated, and enjoy!

### ANOTHER (MORE EXPLICIT) HOW-TO-RUN EXAMPLE

`$ java -jar game.jar v 1 c rad`

### EXAMPLE GAME-PLAY

This example includes parts with INVALID user input, indicated with `# INVALID something`.

```text
rad@box:~$ java -jar game.jar v 0 c rad # INVALID numOfBattles
Usage: game <mode> <numOfBattles> <opponent> <username(s)>
	        mode --- v (versus mode) | s (story mode)
	numOfBattles --- number of battles to fight with opponent (0 < numOfBattles <= 105)
	    opponent --- c (computer) | h (human player)
	 username(s) --- 1 username if opponent=c | 2 usernames if opponent=h (1st username is Challenger)

Usage Example #1: game v 3 c rad
	Example #1 means => play a versus-mode game of 3 battles with 'rad' user as challenger and the computer as the defender.

Usage Example #2: game s 4 h rad ole
	Example #2 means => play a story-mode game of 4 battles with 'rad' user as challenger and 'ole' user as defender.

rad@box:~$ java -jar game.jar v 115 c rad # INVALID numOfBattles
Usage: game <mode> <numOfBattles> <opponent> <username(s)>
	        mode --- v (versus mode) | s (story mode)
	numOfBattles --- number of battles to fight with opponent (0 < numOfBattles <= 105)
	    opponent --- c (computer) | h (human player)
	 username(s) --- 1 username if opponent=c | 2 usernames if opponent=h (1st username is Challenger)

Usage Example #1: game v 3 c rad
	Example #1 means => play a versus-mode game of 3 battles with 'rad' user as challenger and the computer as the defender.

Usage Example #2: game s 4 h rad ole
	Example #2 means => play a story-mode game of 4 battles with 'rad' user as challenger and 'ole' user as defender.

rad@box:~$ java -jar game.jar q 1 c rad # INVALID mode
Usage: game <mode> <numOfBattles> <opponent> <username(s)>
	        mode --- v (versus mode) | s (story mode)
	numOfBattles --- number of battles to fight with opponent (0 < numOfBattles <= 105)
	    opponent --- c (computer) | h (human player)
	 username(s) --- 1 username if opponent=c | 2 usernames if opponent=h (1st username is Challenger)

Usage Example #1: game v 3 c rad
	Example #1 means => play a versus-mode game of 3 battles with 'rad' user as challenger and the computer as the defender.

Usage Example #2: game s 4 h rad ole
	Example #2 means => play a story-mode game of 4 battles with 'rad' user as challenger and 'ole' user as defender.

rad@box:~$ java -jar game.jar v 1 q rad # INVALID opponent
Usage: game <mode> <numOfBattles> <opponent> <username(s)>
	        mode --- v (versus mode) | s (story mode)
	numOfBattles --- number of battles to fight with opponent (0 < numOfBattles <= 105)
	    opponent --- c (computer) | h (human player)
	 username(s) --- 1 username if opponent=c | 2 usernames if opponent=h (1st username is Challenger)

Usage Example #1: game v 3 c rad
	Example #1 means => play a versus-mode game of 3 battles with 'rad' user as challenger and the computer as the defender.

Usage Example #2: game s 4 h rad ole
	Example #2 means => play a story-mode game of 4 battles with 'rad' user as challenger and 'ole' user as defender.

rad@box:~$ java -jar game.jar v 1 c ra`d # INVALID username
> ^C
rad@box:~$ java -jar game.jar v 1 c ra^d # INVALID username
Usage: game <mode> <numOfBattles> <opponent> <username(s)>
	        mode --- v (versus mode) | s (story mode)
	numOfBattles --- number of battles to fight with opponent (0 < numOfBattles <= 105)
	    opponent --- c (computer) | h (human player)
	 username(s) --- 1 username if opponent=c | 2 usernames if opponent=h (1st username is Challenger)

Usage Example #1: game v 3 c rad
	Example #1 means => play a versus-mode game of 3 battles with 'rad' user as challenger and the computer as the defender.

Usage Example #2: game s 4 h rad ole
	Example #2 means => play a story-mode game of 4 battles with 'rad' user as challenger and 'ole' user as defender.

rad@box:~$ java -jar game.jar v 1 c rad
GAME Copyright (C) 2021 William Kibirango

Jicix is a beautiful SENTAUR who dreads sheep. Always has been, and always will be!!!

Eukiu is a disgusting ROBOT who hates books. Always has been, and always will be!!!

Jicix is confronted by Eukiu, who studied their girl! Such insolence! REVENGE!!!

******** Let the Battle... BEGIN!!! *********

========================-----------------------==============================

ARENA: the Xeqah factory
Weapon Damage Effect (WDE): -60
Tribe Energy Effect (TEE)
|   D = -43	|   S = -17	|   H = -8	|   C = 12	|   R = -48
|   A = -17	|   P = 5	|   E = -44	|   B = -21	|   W = -37


(8382636e-ef9f-40ec-bc60-4cce30f2f914) CHALLENGER: Jicix | SENTAUR | 1036

(1b86eb9c-9046-4c86-be98-ee3042037957) DEFENDER: Eukiu | ROBOT | 944

_____________________________________________________________________________

(8382636e-ef9f-40ec-bc60-4cce30f2f914) Jicix, it's time to fight! Strike hard, strike fast and show No MERCY!

EC = Energy Cost
CD = Character Damage
Tribe Damage = D,S,H,C,R,A,P,E,B,W

| EC	| CD	| D	| S	| H	| C	| R	| A	| P	| E	| B	| W	| [id] Name
| -2	| -55	| -60	| -78	| -42	| -86	| -32	| -179	| -172	| -135	| -60	| -36	| [a0] blessings of Euwao
| -3	| -47	| -149	| -166	| -93	| -81	| -62	| -22	| -144	| -30	| -33	| -166	| [a1] curse of Taqob
| -11	| -108	| -158	| -100	| -41	| -198	| -45	| -99	| -180	| -70	| -89	| -117	| [a2] chaos of Uuces
| -8	| -29	| -155	| -117	| -106	| -150	| -164	| -107	| -198	| -185	| -101	| -97	| [a3] curse of Eezor
| -9	| -139	| -174	| -62	| -55	| -24	| -127	| -162	| -61	| -29	| -154	| -68	| [a4] bolt of Cepoa
| -5	| -166	| -181	| -74	| -198	| -193	| -83	| -131	| -169	| -89	| -88	| -92	| [a5] curse of Aanan
| -18	| -72	| -20	| -67	| -142	| -15	| -10	| -171	| -94	| -109	| -12	| -68	| [w0] the Nuqud shield
| -15	| -67	| -143	| -48	| -24	| -24	| -72	| -175	| -96	| -197	| -5	| -82	| [w1] the Dokua spear

Choose what to strike with (e.g. a1): a4


Outstanding!

Jicix attacked Eukiu with bolt of Cepoa.
Eukiu | 944 + -139 + -127 + -48 = 630
Jicix | 1036 + -9 + -17 = 1010

========================-----------------------==============================

ARENA: the Xeqah factory
Weapon Damage Effect (WDE): -60
Tribe Energy Effect (TEE)
|   D = -43	|   S = -17	|   H = -8	|   C = 12	|   R = -48
|   A = -17	|   P = 5	|   E = -44	|   B = -21	|   W = -37


(8382636e-ef9f-40ec-bc60-4cce30f2f914) CHALLENGER: Jicix | SENTAUR | 1010

(1b86eb9c-9046-4c86-be98-ee3042037957) DEFENDER: Eukiu | ROBOT | 630

_____________________________________________________________________________

(1b86eb9c-9046-4c86-be98-ee3042037957) Eukiu, it's time to fight! Strike hard, strike fast and show No MERCY!

EC = Energy Cost
CD = Character Damage
Tribe Damage = D,S,H,C,R,A,P,E,B,W

| EC	| CD	| D	| S	| H	| C	| R	| A	| P	| E	| B	| W	| [id] Name
| -3	| -157	| -57	| -182	| -173	| -116	| -36	| -59	| -62	| -129	| -42	| -78	| [a0] fire of Ralos
| -6	| -102	| -138	| -189	| -75	| -137	| -167	| -60	| -89	| -49	| -81	| -171	| [a1] snare of Jeeuo
| -2	| -1	| -115	| -127	| -194	| -190	| -53	| -136	| -19	| -177	| -86	| -185	| [a2] gust of Xanat
| -1	| -155	| -181	| -129	| -194	| -74	| -47	| -42	| -123	| -20	| -8	| -97	| [a3] gust of Vexah
| -9	| -15	| -2	| -106	| -99	| -56	| -93	| -87	| -51	| -72	| -111	| -33	| [a4] devourer of Hibah
| -2	| -45	| -50	| -89	| -18	| -4	| -141	| -145	| -17	| -5	| -100	| -128	| [a5] hail of Tezai
| -4	| -197	| -41	| -86	| -173	| -145	| -33	| -153	| -169	| -139	| -104	| -111	| [w0] the Yicej machine
| -12	| -50	| -31	| -77	| -26	| -155	| -48	| -16	| -123	| -172	| -111	| -28	| [w1] the Venel mechanism

Choose what to strike with (e.g. a1): COMPUTER chose to strike with a0


Outstanding!

Eukiu attacked Jicix with fire of Ralos.
Jicix | 1010 + -157 + -182 + -17 = 654
Eukiu | 630 + -3 + -48 = 579

========================-----------------------==============================

ARENA: the Xeqah factory
Weapon Damage Effect (WDE): -60
Tribe Energy Effect (TEE)
|   D = -43	|   S = -17	|   H = -8	|   C = 12	|   R = -48
|   A = -17	|   P = 5	|   E = -44	|   B = -21	|   W = -37


(8382636e-ef9f-40ec-bc60-4cce30f2f914) CHALLENGER: Jicix | SENTAUR | 654

(1b86eb9c-9046-4c86-be98-ee3042037957) DEFENDER: Eukiu | ROBOT | 579

_____________________________________________________________________________

(8382636e-ef9f-40ec-bc60-4cce30f2f914) Jicix, it's time to fight! Strike hard, strike fast and show No MERCY!

EC = Energy Cost
CD = Character Damage
Tribe Damage = D,S,H,C,R,A,P,E,B,W

| EC	| CD	| D	| S	| H	| C	| R	| A	| P	| E	| B	| W	| [id] Name
| -2	| -55	| -60	| -78	| -42	| -86	| -32	| -179	| -172	| -135	| -60	| -36	| [a0] blessings of Euwao
| -3	| -47	| -149	| -166	| -93	| -81	| -62	| -22	| -144	| -30	| -33	| -166	| [a1] curse of Taqob
| -11	| -108	| -158	| -100	| -41	| -198	| -45	| -99	| -180	| -70	| -89	| -117	| [a2] chaos of Uuces
| -8	| -29	| -155	| -117	| -106	| -150	| -164	| -107	| -198	| -185	| -101	| -97	| [a3] curse of Eezor
| -9	| -139	| -174	| -62	| -55	| -24	| -127	| -162	| -61	| -29	| -154	| -68	| [a4] bolt of Cepoa
| -5	| -166	| -181	| -74	| -198	| -193	| -83	| -131	| -169	| -89	| -88	| -92	| [a5] curse of Aanan
| -18	| -72	| -20	| -67	| -142	| -15	| -10	| -171	| -94	| -109	| -12	| -68	| [w0] the Nuqud shield
| -15	| -67	| -143	| -48	| -24	| -24	| -72	| -175	| -96	| -197	| -5	| -82	| [w1] the Dokua spear

Choose what to strike with (e.g. a1): a4


Superb!

Jicix attacked Eukiu with bolt of Cepoa.
Eukiu | 579 + -139 + -127 + -48 = 265
Jicix | 654 + -9 + -17 = 628

========================-----------------------==============================

ARENA: the Xeqah factory
Weapon Damage Effect (WDE): -60
Tribe Energy Effect (TEE)
|   D = -43	|   S = -17	|   H = -8	|   C = 12	|   R = -48
|   A = -17	|   P = 5	|   E = -44	|   B = -21	|   W = -37


(8382636e-ef9f-40ec-bc60-4cce30f2f914) CHALLENGER: Jicix | SENTAUR | 628

(1b86eb9c-9046-4c86-be98-ee3042037957) DEFENDER: Eukiu | ROBOT | 265

_____________________________________________________________________________

(1b86eb9c-9046-4c86-be98-ee3042037957) Eukiu, it's time to fight! Strike hard, strike fast and show No MERCY!

EC = Energy Cost
CD = Character Damage
Tribe Damage = D,S,H,C,R,A,P,E,B,W

| EC	| CD	| D	| S	| H	| C	| R	| A	| P	| E	| B	| W	| [id] Name
| -3	| -157	| -57	| -182	| -173	| -116	| -36	| -59	| -62	| -129	| -42	| -78	| [a0] fire of Ralos
| -6	| -102	| -138	| -189	| -75	| -137	| -167	| -60	| -89	| -49	| -81	| -171	| [a1] snare of Jeeuo
| -2	| -1	| -115	| -127	| -194	| -190	| -53	| -136	| -19	| -177	| -86	| -185	| [a2] gust of Xanat
| -1	| -155	| -181	| -129	| -194	| -74	| -47	| -42	| -123	| -20	| -8	| -97	| [a3] gust of Vexah
| -9	| -15	| -2	| -106	| -99	| -56	| -93	| -87	| -51	| -72	| -111	| -33	| [a4] devourer of Hibah
| -2	| -45	| -50	| -89	| -18	| -4	| -141	| -145	| -17	| -5	| -100	| -128	| [a5] hail of Tezai
| -4	| -197	| -41	| -86	| -173	| -145	| -33	| -153	| -169	| -139	| -104	| -111	| [w0] the Yicej machine
| -12	| -50	| -31	| -77	| -26	| -155	| -48	| -16	| -123	| -172	| -111	| -28	| [w1] the Venel mechanism

Choose what to strike with (e.g. a1): COMPUTER chose to strike with w0


Excellent!

Eukiu used the Yicej machine on Jicix.
Jicix | 628 + -197 + -86 + -17 = 268
Eukiu | 265 + -4 + -48 = 213

========================-----------------------==============================

ARENA: the Xeqah factory
Weapon Damage Effect (WDE): -60
Tribe Energy Effect (TEE)
|   D = -43	|   S = -17	|   H = -8	|   C = 12	|   R = -48
|   A = -17	|   P = 5	|   E = -44	|   B = -21	|   W = -37


(8382636e-ef9f-40ec-bc60-4cce30f2f914) CHALLENGER: Jicix | SENTAUR | 268

(1b86eb9c-9046-4c86-be98-ee3042037957) DEFENDER: Eukiu | ROBOT | 213

_____________________________________________________________________________

(8382636e-ef9f-40ec-bc60-4cce30f2f914) Jicix, it's time to fight! Strike hard, strike fast and show No MERCY!

EC = Energy Cost
CD = Character Damage
Tribe Damage = D,S,H,C,R,A,P,E,B,W

| EC	| CD	| D	| S	| H	| C	| R	| A	| P	| E	| B	| W	| [id] Name
| -2	| -55	| -60	| -78	| -42	| -86	| -32	| -179	| -172	| -135	| -60	| -36	| [a0] blessings of Euwao
| -3	| -47	| -149	| -166	| -93	| -81	| -62	| -22	| -144	| -30	| -33	| -166	| [a1] curse of Taqob
| -11	| -108	| -158	| -100	| -41	| -198	| -45	| -99	| -180	| -70	| -89	| -117	| [a2] chaos of Uuces
| -8	| -29	| -155	| -117	| -106	| -150	| -164	| -107	| -198	| -185	| -101	| -97	| [a3] curse of Eezor
| -9	| -139	| -174	| -62	| -55	| -24	| -127	| -162	| -61	| -29	| -154	| -68	| [a4] bolt of Cepoa
| -5	| -166	| -181	| -74	| -198	| -193	| -83	| -131	| -169	| -89	| -88	| -92	| [a5] curse of Aanan
| -18	| -72	| -20	| -67	| -142	| -15	| -10	| -171	| -94	| -109	| -12	| -68	| [w0] the Nuqud shield
| -15	| -67	| -143	| -48	| -24	| -24	| -72	| -175	| -96	| -197	| -5	| -82	| [w1] the Dokua spear

Choose what to strike with (e.g. a1): a4


Excellent!

Jicix attacked Eukiu with bolt of Cepoa.
Eukiu | 213 + -139 + -127 + -48 = -101
Jicix | 268 + -9 + -17 = 242

***> (8382636e-ef9f-40ec-bc60-4cce30f2f914) Jicix Wins! <***

Congrats rad (8382636e-ef9f-40ec-bc60-4cce30f2f914)! YOU WIN!

rad@box:~$
```