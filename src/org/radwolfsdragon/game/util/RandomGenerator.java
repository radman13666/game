package org.radwolfsdragon.game.util;

import org.radwolfsdragon.game.base.Character;
import org.radwolfsdragon.game.base.Tribe;
import org.radwolfsdragon.game.base.Weapon;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public final class RandomGenerator {
    private RandomGenerator() {}

    /** Generate a random character between ch1 and ch2 */
    public static char getRandomCharacter(char ch1, char ch2) {
        return (char)(ch1 + Math.random() * (ch2 - ch1 + 1));
    }
    /** Generate a random lowercase letter */
    public static char getRandomLowerCaseLetter() {
        return getRandomCharacter('a', 'z');
    }
    /** Generate a random uppercase letter */
    public static char getRandomUpperCaseLetter() {
        return getRandomCharacter('A', 'Z');
    }

    public static char getRandomVowel() {
        char[] vowels = {'a','e','i','o','u'};
        int randomIndex = generateRandomNonNegativeInteger(vowels.length);
        return vowels[randomIndex];
    }

    public static int generateRandomNonNegativeInteger(int maxValue) {
        // 0 <= x < MAX
        return (int) (Math.random() * maxValue);
    }

    public static int generateRandomNegativeInteger(int minValue) {
        // (-1 * MIN) < x < 0
        int i;
        do {
            i = generateRandomNonNegativeInteger(minValue);
        } while (i == 0);
        return -1 * i;
    }

    public static int generateRandomInteger(int maxValue) {
        // (-1 * MAX) <= x < MAX
        return (-1 * maxValue) + (int) (Math.random() * 2 * maxValue);
    }

    public static boolean generateRandomBoolean() {
        return generateRandomInteger(5) > 0;
    }

    public static Map<String, Integer> generateRandomTribeDamage() {
        Map<String, Integer> m = new HashMap<>();
        m.put(Tribe.DRAGON.name(), generateRandomNegativeInteger(Constants.MAX_TRIBE_DAMAGE));
        m.put(Tribe.SENTAUR.name(), generateRandomNegativeInteger(Constants.MAX_TRIBE_DAMAGE));
        m.put(Tribe.HUMAN.name(), generateRandomNegativeInteger(Constants.MAX_TRIBE_DAMAGE));
        m.put(Tribe.CYCLOPS.name(), generateRandomNegativeInteger(Constants.MAX_TRIBE_DAMAGE));
        m.put(Tribe.ROBOT.name(), generateRandomNegativeInteger(Constants.MAX_TRIBE_DAMAGE));
        m.put(Tribe.ALIEN.name(), generateRandomNegativeInteger(Constants.MAX_TRIBE_DAMAGE));
        m.put(Tribe.PREDATOR.name(), generateRandomNegativeInteger(Constants.MAX_TRIBE_DAMAGE));
        m.put(Tribe.ELF.name(), generateRandomNegativeInteger(Constants.MAX_TRIBE_DAMAGE));
        m.put(Tribe.BLOB.name(), generateRandomNegativeInteger(Constants.MAX_TRIBE_DAMAGE));
        m.put(Tribe.WATER_BEAST.name(), generateRandomNegativeInteger(Constants.MAX_TRIBE_DAMAGE));
        m.put(Tribe.CHARACTER.name(), generateRandomNegativeInteger(Constants.MAX_TRIBE_DAMAGE));
        return m;
    }

    public static Map<String, Integer> generateRandomTribeEnergyEffects() {
        Map<String, Integer> m = new HashMap<>();
        m.put(Tribe.DRAGON.name(), generateRandomInteger(Constants.MAX_TRIBE_ENERGY_EFFECT));
        m.put(Tribe.SENTAUR.name(),generateRandomInteger(Constants.MAX_TRIBE_ENERGY_EFFECT));
        m.put(Tribe.HUMAN.name(), generateRandomInteger(Constants.MAX_TRIBE_ENERGY_EFFECT));
        m.put(Tribe.CYCLOPS.name(),generateRandomInteger(Constants.MAX_TRIBE_ENERGY_EFFECT));
        m.put(Tribe.ROBOT.name(), generateRandomInteger(Constants.MAX_TRIBE_ENERGY_EFFECT));
        m.put(Tribe.ALIEN.name(), generateRandomInteger(Constants.MAX_TRIBE_ENERGY_EFFECT));
        m.put(Tribe.PREDATOR.name(), generateRandomInteger(Constants.MAX_TRIBE_ENERGY_EFFECT));
        m.put(Tribe.ELF.name(), generateRandomInteger(Constants.MAX_TRIBE_ENERGY_EFFECT));
        m.put(Tribe.BLOB.name(), generateRandomInteger(Constants.MAX_TRIBE_ENERGY_EFFECT));
        m.put(Tribe.WATER_BEAST.name(), generateRandomInteger(Constants.MAX_TRIBE_ENERGY_EFFECT));
        m.put(Tribe.CHARACTER.name(), 0);
        return m;
    }

    public static String generateRandomFiveLetterName() {
        return String.valueOf(RandomGenerator.getRandomUpperCaseLetter()) +
                RandomGenerator.getRandomVowel() +
                RandomGenerator.getRandomLowerCaseLetter() +
                RandomGenerator.getRandomVowel() +
                RandomGenerator.getRandomLowerCaseLetter();
    }

    public static List<Weapon> getRandomlyPickedWeaponsAsList(List<Weapon> weapons, int numOfWeaponsToPick) {
        List<Weapon> pickedWeapons = new ArrayList<>();
        for (int i = 0; i < numOfWeaponsToPick; i++) {
            int j = generateRandomNonNegativeInteger(weapons.size());
            pickedWeapons.add(weapons.remove(j));
        }
        return pickedWeapons;
    }

    // backstory for fighters in each battle (, and story-line for each battle ONLY IN STORY MODE --
    // to give the challenger a reason to fight the defender).

    public static String generateRandomStoryline(Character character, Character opponent) {
        int singular = 0;
        String act = Constants.ACTIONS_PAST_TENSE[generateRandomNonNegativeInteger(Constants.ACTIONS_PAST_TENSE.length)];
        String object = Constants.OBJECTS_SINGULAR_PLURAL[
                generateRandomNonNegativeInteger(Constants.OBJECTS_SINGULAR_PLURAL.length)
                ][singular];

        return String.format("%s is confronted by %s, who %s their %s! Such insolence! REVENGE!!!",
                character.getName(), opponent.getName(), act, object);
    }

    public static String generateRandomBackstory(Character character) {
        int plural = 1;
        String adjective = Constants.ADJECTIVES[generateRandomNonNegativeInteger(Constants.ADJECTIVES.length)];
        String emotion = Constants.EMOTIONS_PRESENT_CONTINUOUS_TENSE[
                generateRandomNonNegativeInteger(Constants.EMOTIONS_PRESENT_CONTINUOUS_TENSE.length)
                ];
        String object = Constants.OBJECTS_SINGULAR_PLURAL[
                generateRandomNonNegativeInteger(Constants.OBJECTS_SINGULAR_PLURAL.length)
                ][plural];

        return String.format("%s is a %s %s who %s %s. Always has been, and always will be!!!",
                character.getName(), adjective, character.getTribe(), emotion, object);
    }
}
