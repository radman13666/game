package org.radwolfsdragon.game.util;

import org.radwolfsdragon.game.base.Tribe;

public final class Constants {
    public static final int MAX_ATTACK_ENERGY_COST = 12;
    public static final int MAX_WEAPON_ENERGY_COST = 20;
    public static final int MAX_TRIBE_DAMAGE = 200;
    public static final int MAX_TRIBE_ENERGY_EFFECT = 50;
    public static final int MAX_WORLD_WEAPON_CHARACTER_DAMAGE = 80;
    public static final int MAX_CHARACTER_ENERGY = 2000;
    public static final int MAX_NUM_OF_BATTLES = 105;
    public static final int MAX_USERNAME_LENGTH = 10;
    public static final int MAX_STORYMODE_POWERUP = 150;
    public static final int MAX_COMPUTER_THINKING_TIME = 15; // seconds
    public static final Tribe[] TRIBES = Tribe.values();

    public static final int CHALLENGER = 0;
    public static final int DEFENDER = 1;

    public static final int MAX_NUM_OF_ATTACKS_PER_TRIBE = 3;
    public static final int MAX_NUM_OF_ATTACKS_PER_CHARACTER = MAX_NUM_OF_ATTACKS_PER_TRIBE;
    public static final int MAX_NUM_OF_WEAPONS_PER_CHARACTER = 2;
    public static final char WEAPON_STRIKE_CHOICE = 'w';

    public static final String[] ADJECTIVES = {
            "ugly",
            "disgusting",
            "terrifying",
            "old",
            "wise",
            "beautiful",
            "angelic",
            "holy",
            "godly",
            "devilish",
            "loving",
            "sweet",
            "wicked",
            "cool",
            "arrogant",
            "young",
            "brave",
            "coward"
    };

    public static final String[] EMOTIONS_PRESENT_CONTINUOUS_TENSE = {
            "loves",
            "hates",
            "adores",
            "fears",
            "dreads",
    };

    public static final String[][] OBJECTS_SINGULAR_PLURAL = {
            {"bird", "birds"},
            {"girl", "girls"},
            {"sheep", "sheep"},
            {"boy", "boys"},
            {"baby", "babies"},
            {"pig", "pigs"},
            {"diary", "diaries"},
            {"book", "books"},
    };

    public static final String[] ACTIONS_PAST_TENSE = {
            "attacked",
            "sneezed",
            "caressed",
            "wagged",
            "killed",
            "rescued",
            "saved",
            "studied",
            "read",
            "kicked"
    };

    public static final String[] BATTLE_COMPLIMENTS = {
            "Superb!",
            "Outstanding!",
            "Excellent!",
            "Sugoi!",
            "HAHAHAHAAAA YES! FINISH HIM!"
    };

    private Constants() {}
}
