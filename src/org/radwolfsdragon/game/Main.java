package org.radwolfsdragon.game;

import org.radwolfsdragon.game.base.Match;
import org.radwolfsdragon.game.base.MatchFactory;
import org.radwolfsdragon.game.base.Player;
import org.radwolfsdragon.game.util.Constants;

public class Main {

    public static void main(String[] args) {
        Match match = null;

        if (validModeArg(args) && validNumOfBattlesArg(args) && validOpponentArg(args) && validUsernameArgs(args)) {
            displayBanner();

            String modeStr = args[0];
            int numOfBattles = Integer.parseInt(args[1]);
            String opponentStr = args[2];
            String challengerUsername = args[3];

            Match.Mode mode = modeStr.equals("v") ? Match.Mode.VERSUS : Match.Mode.STORY;

            if (opponentStr.equals("c")) {
                match = MatchFactory.newMatchAgainstComputer(mode, challengerUsername, numOfBattles);
            } else {
                String defenderUsername = args[4];
                match = MatchFactory.newMatchAmongstHumanPlayers(mode, challengerUsername, defenderUsername, numOfBattles);
            }
        } else {
            displayHelpMessage();
            System.exit(1);
        }

        Player victor = match.play();
        System.out.printf("Congrats %s (%s)! YOU WIN!\n\n", victor.getUsername(), victor.getUUID());
    }

    private static void displayBanner() {
        System.out.println("GAME Copyright (C) 2021 William Kibirango\n");
    }

    private static boolean validModeArg(String[] args) {
        if (args.length > 0) {
            String modeStr = args[0];
            return modeStr.length() == 1 &&
                    (modeStr.equals("v") || modeStr.equals("s"));
        } else return false;
    }

    private static boolean validNumOfBattlesArg(String[] args) {
        if (args.length > 1) {
            try {
                int numOfBattles = Integer.parseInt(args[1]);
                return numOfBattles > 0 && numOfBattles <= Constants.MAX_NUM_OF_BATTLES;
            } catch (NumberFormatException e) {
                System.err.printf("numOfBattles is not valid: '%s'\n\n", args[1]);
                return false;
            }
        } else return false;
    }

    private static boolean validOpponentArg(String[] args) {
        if (args.length > 2) {
            String opponentStr = args[2];
            return opponentStr.length() == 1 &&
                    (opponentStr.equals("c") || opponentStr.equals("h"));
        } else return false;
    }

    private static boolean validUsernameArgs(String[] args) {
        if (args.length > 3) {
            if (args.length == 4) {
                return validUsername(args[3]);
            } else if (args.length == 5) {
                return validUsername(args[3]) && validUsername(args[4]) && !args[3].equals(args[4]);
            } else return false;
        } else return false;
    }

    private static boolean validUsername(String username) {
        return !username.isBlank() && username.length() <= Constants.MAX_USERNAME_LENGTH && validCharacters(username);
    }

    private static boolean validCharacters(String username) {
        boolean valid = true;
        char[] a = username.toCharArray();

        for (char c: a) {
            valid = ((c >= 'a') && (c <= 'z')) || ((c >= 'A') && (c <= 'Z')) || ((c >= '0') && (c <= '9'));
            if (!valid) break;
        }

        return valid;
    }

    private static void displayHelpMessage() {
        System.out.println("Usage: game <mode> <numOfBattles> <opponent> <username(s)>\n"+
                "\t        mode --- v (versus mode) | s (story mode)\n"+
                "\tnumOfBattles --- number of battles to fight with opponent (0 < numOfBattles <= 105)\n"+
                "\t    opponent --- c (computer) | h (human player)\n"+
                "\t username(s) --- 1 username if opponent=c | 2 usernames if opponent=h (1st username is Challenger)\n\n"+
                "Usage Example #1: game v 3 c rad\n"+
                "\tExample #1 means => play a versus-mode game of 3 battles with 'rad' user as challenger and the computer as the defender.\n\n"+
                "Usage Example #2: game s 4 h rad ole\n"+
                "\tExample #2 means => play a story-mode game of 4 battles with 'rad' user as challenger and 'ole' user as defender.\n");
    }
}
