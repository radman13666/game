package org.radwolfsdragon.game.base;

import org.radwolfsdragon.game.util.Constants;

import java.util.ArrayList;
import java.util.List;

@SuppressWarnings("unchecked")
public final class MatchFactory {

    private MatchFactory() {}

    public static Match newMatchAgainstComputer(Match.Mode mode, String humanPlayerUsername, int numOfBattles) {
        Object[] results1 = firstInitialisation(mode, humanPlayerUsername, Battle.Opponent.COMPUTER.name(), numOfBattles);

        switch (mode) {
            case STORY:
                return new StoryModeMatch(
                        (List<Player>) results1[0],
                        (List<Character>) results1[1],
                        (List<World>) results1[2],
                        numOfBattles,
                        Battle.Opponent.COMPUTER
                );
            case VERSUS:
            default:
                return new VersusModeMatch(
                        (List<Player>) results1[0],
                        (List<Character>) results1[1],
                        (List<World>) results1[2],
                        numOfBattles,
                        Battle.Opponent.COMPUTER
                );
        }
    }

    public static Match newMatchAmongstHumanPlayers(
            Match.Mode mode, String humanChallengerUsername, String humanDefenderUsername, int numOfBattles) {
        Object[] results1 = firstInitialisation(mode, humanChallengerUsername, humanDefenderUsername, numOfBattles);

        switch (mode) {
            case STORY:
                return new StoryModeMatch(
                        (List<Player>) results1[0],
                        (List<Character>) results1[1],
                        (List<World>) results1[2],
                        numOfBattles,
                        Battle.Opponent.HUMAN_PLAYER
                );
            case VERSUS:
            default:
                return new VersusModeMatch(
                        (List<Player>) results1[0],
                        (List<Character>) results1[1],
                        (List<World>) results1[2],
                        numOfBattles,
                        Battle.Opponent.HUMAN_PLAYER
                );
        }
    }

    private static Object[] firstInitialisation(
            Match.Mode mode, String challengerUsername, String defenderUsername, int numOfBattles) {
        List<Player> players = generatePlayers(challengerUsername, defenderUsername);
        List<World> arenas = WorldFactory.generateRandomWorlds(numOfBattles);

        int numOfWeapons;
        List<Character> characters;

        if (mode == Match.Mode.STORY) {
            numOfWeapons = Constants.MAX_NUM_OF_WEAPONS_PER_CHARACTER * (numOfBattles + 1);
            characters = CharacterFactory.generateRandomCharacters(
                    numOfBattles + 1,
                    AttackFactory.generateRandomTribeAttacks(),
                    WeaponFactory.generateRandomWeapons(numOfWeapons)
            );
        } else {
            numOfWeapons = 2 * Constants.MAX_NUM_OF_WEAPONS_PER_CHARACTER * numOfBattles;
            characters = CharacterFactory.generateRandomCharacters(
                    2 * numOfBattles,
                    AttackFactory.generateRandomTribeAttacks(),
                    WeaponFactory.generateRandomWeapons(numOfWeapons)
            );
        }

        Object[] results = new Object[3];
        results[0] = players;
        results[1] = characters;
        results[2] = arenas;

        return results;
    }

    private static List<Player> generatePlayers(String challengerUsername, String defenderUsername) {
        List<Player> players = new ArrayList<>();

        players.add(new Player(challengerUsername));
        players.add(new Player(defenderUsername));

        return players;
    }
}
