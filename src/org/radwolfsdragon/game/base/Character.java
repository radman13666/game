package org.radwolfsdragon.game.base;

public interface Character {
    void strikeWithWeapon(int weapon, Character victim, World arena);
    void strikeWithAttack(int attack, Character victim, World arena);
    int getEnergy();
    void setEnergy(int newEnergy);
    Tribe getTribe();
    String getName();
    Attack[] getAttacks();
    Weapon[] getWeapons();
}
