package org.radwolfsdragon.game.base;

import org.radwolfsdragon.game.util.Constants;
import org.radwolfsdragon.game.util.RandomGenerator;

import java.util.List;
import java.util.Scanner;

final class BattleImpl implements Battle {

    private final List<Fighter> fighters;
    private final World arena;
    private int turnCounter;
    private Opponent opponent;
    private Match.Mode mode;

    BattleImpl(List<Fighter> fighters, World arena) {
        this.fighters = fighters;
        this.arena = arena;
        this.turnCounter = 0;
    }

    @Override
    public Fighter fight(Opponent opponent, Match.Mode mode) {
        this.opponent = opponent;
        this.mode = mode;
        displayStories();
        System.out.println("******** Let the Battle... BEGIN!!! *********\n");

        do {
            Object[] choice = chooseStrike();
            displayBattleCompliment();
            boolean withWeapon = (Boolean) choice[0];
            int strikeIndex = (Integer) choice[1];

            if (challengersTurn()) {
                if (withWeapon) {
                    fighters.get(Constants.CHALLENGER).getFightingCreature().strikeWithWeapon(
                            strikeIndex,
                            fighters.get(Constants.DEFENDER).getFightingCreature(),
                            this.arena
                    );
                } else {
                    fighters.get(Constants.CHALLENGER).getFightingCreature().strikeWithAttack(
                            strikeIndex,
                            fighters.get(Constants.DEFENDER).getFightingCreature(),
                            this.arena
                    );
                }
            } else {
                if (withWeapon) {
                    fighters.get(Constants.DEFENDER).getFightingCreature().strikeWithWeapon(
                            strikeIndex,
                            fighters.get(Constants.CHALLENGER).getFightingCreature(),
                            this.arena
                    );
                } else {
                    fighters.get(Constants.DEFENDER).getFightingCreature().strikeWithAttack(
                            strikeIndex,
                            fighters.get(Constants.CHALLENGER).getFightingCreature(),
                            this.arena
                    );
                }
            }

            incrementTurnCounter();
        } while (fighters.get(Constants.CHALLENGER).getFightingCreature().getEnergy() > 0 &&
                fighters.get(Constants.DEFENDER).getFightingCreature().getEnergy() > 0);

        return fighters.get(Constants.CHALLENGER).getFightingCreature().getEnergy() > 0 ?
                fighters.get(Constants.CHALLENGER) :
                fighters.get(Constants.DEFENDER);
    }

    private void incrementTurnCounter() {
        this.turnCounter++;
    }

    private boolean challengersTurn() {
        return this.turnCounter % 2 == 0;
    }

    private Object[] chooseStrike() {
        displayCurrentBattleStats();

        if (this.opponent == Opponent.HUMAN_PLAYER) {
            if (challengersTurn()) {
                displayStats(fighters.get(Constants.CHALLENGER));
            } else {
                displayStats(fighters.get(Constants.DEFENDER));
            }
            return getHumanStrike();
        } else {
            if (challengersTurn()) {
                displayStats(fighters.get(Constants.CHALLENGER));
                return getHumanStrike();
            } else { // computer chooses its strike (at random, for now)
                displayStats(fighters.get(Constants.DEFENDER));
                simulateComputerThinking();

                Object[] choice = new Object[2];
                boolean withWeapon = RandomGenerator.generateRandomBoolean();
                choice[0] = withWeapon;
                int i;
                if (withWeapon) {
                    int numOfWeapons = fighters.get(Constants.DEFENDER).getFightingCreature().getWeapons().length;
                    i = RandomGenerator.generateRandomNonNegativeInteger(numOfWeapons);
                } else {
                    int numOfAttacks = fighters.get(Constants.DEFENDER).getFightingCreature().getAttacks().length;
                    i = RandomGenerator.generateRandomNonNegativeInteger(numOfAttacks);
                }

                char w = withWeapon ? 'w' : 'a';
                choice[1] = i;
                System.out.print(Opponent.COMPUTER + " chose to strike with " + w + i + '\n');

                return choice;
            }
        }
    }

    private void simulateComputerThinking() {
        try { // think for 0s <= time < 15s
            Thread.sleep((long) RandomGenerator.generateRandomNonNegativeInteger(Constants.MAX_COMPUTER_THINKING_TIME) * 1000);
        } catch (InterruptedException ignored) {}
    }

    private Object[] getHumanStrike() {
        Scanner scanner = new Scanner(System.in);
        String choiceStr = scanner.nextLine();

        char[] choiceChars;
        if (validHumanStrike(choiceStr)) {
            choiceChars = choiceStr.toCharArray();
        } else {
            choiceChars = new char[]{'a', '1'}; // the default choice of strike
        }

        Object[] choice = new Object[2];
        choice[0] = choiceChars[0] == Constants.WEAPON_STRIKE_CHOICE;
        choice[1] = java.lang.Character.digit(choiceChars[1], 10);
        return choice;
    }

    private boolean validHumanStrike(String choiceInput) {
        if (choiceInput.length() == 2) {
            char[] c = choiceInput.toCharArray();
            return (c[0] == 'a' || c[0] == 'w') && java.lang.Character.isDigit(c[1]);
        } else return false;
    }

    private void displayCurrentBattleStats() {
        // world stats
        System.out.printf("========================-----------------------==============================\n\n" +
                "ARENA: %s\nWeapon Damage Effect (WDE): %d\nTribe Energy Effect (TEE)\n" +
                        "|   D = %d\t|   S = %d\t|   H = %d\t|   C = %d\t|   R = %d\n" +
                        "|   A = %d\t|   P = %d\t|   E = %d\t|   B = %d\t|   W = %d\n\n\n",
                arena.getName(), arena.getWeaponCharacterDamageEffect(),
                arena.getEnergyEffect(Tribe.DRAGON),
                arena.getEnergyEffect(Tribe.SENTAUR),
                arena.getEnergyEffect(Tribe.HUMAN),
                arena.getEnergyEffect(Tribe.CYCLOPS),
                arena.getEnergyEffect(Tribe.ROBOT),
                arena.getEnergyEffect(Tribe.ALIEN),
                arena.getEnergyEffect(Tribe.PREDATOR),
                arena.getEnergyEffect(Tribe.ELF),
                arena.getEnergyEffect(Tribe.BLOB),
                arena.getEnergyEffect(Tribe.WATER_BEAST));

        // fighters' general stats
        String cId = fighters.get(Constants.CHALLENGER).getPlayerUUID();
        Character c = fighters.get(Constants.CHALLENGER).getFightingCreature();
        System.out.printf("(%s) CHALLENGER: %s | %s | %d\n\n", cId, c.getName(), c.getTribe(), c.getEnergy());

        String dId = fighters.get(Constants.DEFENDER).getPlayerUUID();
        Character d = fighters.get(Constants.DEFENDER).getFightingCreature();
        System.out.printf("(%s) DEFENDER: %s | %s | %d\n\n", dId, d.getName(), d.getTribe(), d.getEnergy());
    }

    private void displayStats(Fighter fighter) {
        String id = fighter.getPlayerUUID();
        Character c = fighter.getFightingCreature();
        System.out.printf("_____________________________________________________________________________\n\n" +
                "(%s) %s, it's time to fight! Strike hard, strike fast and show No MERCY!\n\n", id, c.getName());

        System.out.println("EC = Energy Cost\nCD = Character Damage\n" +
                "Tribe Damage = D,S,H,C,R,A,P,E,B,W\n");

        System.out.println("| EC\t| CD\t| D\t| S\t| H\t| C\t| R\t| A\t| P\t| E\t| B\t| W\t| [id] Name");

        String attackListing = "| %d\t| %d\t| %d\t| %d\t| %d\t| %d\t| %d\t| %d\t| %d\t| %d\t| %d\t| %d\t";

        for (int i = 0; i < c.getAttacks().length; i++) {
            Attack a = c.getAttacks()[i];
            System.out.printf(attackListing + "| [a%d] %s\n",
                    a.getEnergyCost(),
                    a.getDamage(Tribe.CHARACTER),
                    a.getDamage(Tribe.DRAGON),
                    a.getDamage(Tribe.SENTAUR),
                    a.getDamage(Tribe.HUMAN),
                    a.getDamage(Tribe.CYCLOPS),
                    a.getDamage(Tribe.ROBOT),
                    a.getDamage(Tribe.ALIEN),
                    a.getDamage(Tribe.PREDATOR),
                    a.getDamage(Tribe.ELF),
                    a.getDamage(Tribe.BLOB),
                    a.getDamage(Tribe.WATER_BEAST),
                    i, a.getName());
        }

        for (int i = 0; i < c.getWeapons().length; i++) {
            Weapon w = c.getWeapons()[i];
            System.out.printf(attackListing + "| [w%d] %s\n",
                    w.getEnergyCost(),
                    w.getDamage(Tribe.CHARACTER),
                    w.getDamage(Tribe.DRAGON),
                    w.getDamage(Tribe.SENTAUR),
                    w.getDamage(Tribe.HUMAN),
                    w.getDamage(Tribe.CYCLOPS),
                    w.getDamage(Tribe.ROBOT),
                    w.getDamage(Tribe.ALIEN),
                    w.getDamage(Tribe.PREDATOR),
                    w.getDamage(Tribe.ELF),
                    w.getDamage(Tribe.BLOB),
                    w.getDamage(Tribe.WATER_BEAST),
                    i, w.getName());
        }

        System.out.print("\nChoose what to strike with (e.g. a1): ");
    }

    private void displayStories() {
        if (this.mode == Match.Mode.VERSUS) {
            System.out.println(RandomGenerator.generateRandomBackstory(fighters.get(Constants.CHALLENGER).getFightingCreature())+'\n');
        }
        System.out.println(RandomGenerator.generateRandomBackstory(fighters.get(Constants.DEFENDER).getFightingCreature())+'\n');
        System.out.println(RandomGenerator.generateRandomStoryline(
                fighters.get(Constants.CHALLENGER).getFightingCreature(),
                fighters.get(Constants.DEFENDER).getFightingCreature()
        )+'\n');
    }

    private void displayBattleCompliment() {
        System.out.println("\n\n"+
                Constants.BATTLE_COMPLIMENTS[
                        RandomGenerator.generateRandomNonNegativeInteger(Constants.BATTLE_COMPLIMENTS.length)]+'\n'
        );
    }
}
