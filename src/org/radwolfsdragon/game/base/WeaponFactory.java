package org.radwolfsdragon.game.base;

import org.radwolfsdragon.game.util.Constants;
import org.radwolfsdragon.game.util.RandomGenerator;

import java.util.ArrayList;
import java.util.List;

final class WeaponFactory {

    private static final String[] DESCRIPTION = {
            "sword",
            "shield",
            "staff",
            "laser gun",
            "tank",
            "armour",
            "spear",
            "mechanism",
            "machine"
    };

    private WeaponFactory() {}

    static Weapon generateRandomWeapon() {
        int randomIndex = RandomGenerator.generateRandomNonNegativeInteger(DESCRIPTION.length);

        return new WeaponImpl("the " + RandomGenerator.generateRandomFiveLetterName() + " " + DESCRIPTION[randomIndex],
                RandomGenerator.generateRandomTribeDamage(),
                -1 * RandomGenerator.generateRandomNonNegativeInteger(Constants.MAX_WEAPON_ENERGY_COST)
        );
    }

    static List<Weapon> generateRandomWeapons(int numOfWeapons) {
        List<Weapon> weapons = new ArrayList<>();
        for (int i = 0; i < numOfWeapons; i++) {
            weapons.add(i, generateRandomWeapon());
        }
        return weapons;
    }
}
