package org.radwolfsdragon.game.base;

import org.radwolfsdragon.game.util.Constants;
import org.radwolfsdragon.game.util.RandomGenerator;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

final class CharacterFactory {

    private CharacterFactory() {}

    private static List<Attack> filterNonTribeAttacks(List<Attack> attacks, Tribe tribe) {
        List<Attack> l = new ArrayList<>();
        for (Attack attack : attacks) {
            if (attack.getTribe() == tribe) {
                l.add(attack);
            }
        }
        return l;
    }

    static Character generateRandomCharacter(
            Tribe tribe, List<Attack> tribeAttacks, List<Weapon> weapons) {
        List<Attack> filteredAttacks = filterNonTribeAttacks(tribeAttacks, tribe);
        filteredAttacks.addAll(AttackFactory.generateRandomAttacks(Constants.MAX_NUM_OF_ATTACKS_PER_CHARACTER, Tribe.CHARACTER));

        return new CharacterImpl(RandomGenerator.generateRandomFiveLetterName(),
                tribe, RandomGenerator.generateRandomNonNegativeInteger(Constants.MAX_CHARACTER_ENERGY),
                filteredAttacks, weapons
        );
    }

    static List<Character> generateRandomCharacters(
            int numOfCharacters, Map<String, List<Attack>> allTribeAttacks, List<Weapon> allWeapons) {

        List<Character> characters = new ArrayList<>();

        int i = 0;
        while (i < numOfCharacters) {
            int j = RandomGenerator.generateRandomNonNegativeInteger(Constants.TRIBES.length);
            Tribe t = Constants.TRIBES[j];
            if (t != Tribe.CHARACTER) {
                List<Attack> a = allTribeAttacks.get(t.name());
                List<Weapon> w = RandomGenerator.getRandomlyPickedWeaponsAsList(allWeapons, Constants.MAX_NUM_OF_WEAPONS_PER_CHARACTER);
                characters.add(i++, generateRandomCharacter(t, a, w));
            }
        }
        return characters;
    }
 }
