package org.radwolfsdragon.game.base;

final class Fighter {
    private final String playerUUID;
    private final Character creature;

    Fighter(String playerUUID, Character creature) {
        this.playerUUID = playerUUID;
        this.creature = creature;
    }

    public Character getFightingCreature() {
        return this.creature;
    }

    public String getPlayerUUID() {
        return this.playerUUID;
    }
}
