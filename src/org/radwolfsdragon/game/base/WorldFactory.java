package org.radwolfsdragon.game.base;

import org.radwolfsdragon.game.util.Constants;
import org.radwolfsdragon.game.util.RandomGenerator;

import java.util.ArrayList;
import java.util.List;

final class WorldFactory {
    private static final String[] DESCRIPTION = {
            "planet",
            "river",
            "forest",
            "plains",
            "tower",
            "sky temple",
            "ponds",
            "ocean",
            "factory",
            "swamp",
            "caves",
            "pass",
            "mountain",
            "dungeon",
            "sewer",
            "camp site"
    };

    private WorldFactory() {}

    static World generateRandomWorld() {
        int i = RandomGenerator.generateRandomNonNegativeInteger(DESCRIPTION.length);

        return new WorldImpl("the " + RandomGenerator.generateRandomFiveLetterName() + " " + DESCRIPTION[i],
                -1 * RandomGenerator.generateRandomNonNegativeInteger(Constants.MAX_WORLD_WEAPON_CHARACTER_DAMAGE),
                RandomGenerator.generateRandomTribeEnergyEffects());
    }

    static List<World> generateRandomWorlds(int numOfWorlds) {
        List<World> worlds = new ArrayList<>();
        for (int i = 0; i < numOfWorlds; i++) {
            worlds.add(i, generateRandomWorld());
        }
        return worlds;
    }
}
