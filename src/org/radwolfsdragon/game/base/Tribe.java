package org.radwolfsdragon.game.base;

public enum Tribe {
    DRAGON(1000),
    SENTAUR(300),
    HUMAN(50),
    CYCLOPS(200),
    ROBOT(500),
    ALIEN(400),
    PREDATOR(400),
    ELF(250),
    BLOB(200),
    WATER_BEAST(250),
    CHARACTER(0); // utility "tribe"


    private final int energy;

    Tribe(int energy) {
        this.energy = energy;
    }

    public int getEnergy() {
        return energy;
    }
}
