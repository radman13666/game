package org.radwolfsdragon.game.base;

public interface Battle {
    enum Opponent {
        HUMAN_PLAYER,
        COMPUTER
    }

    Fighter fight(Opponent opponent, Match.Mode mode);
}
