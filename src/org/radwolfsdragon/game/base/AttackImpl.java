package org.radwolfsdragon.game.base;

import java.util.Map;

final class AttackImpl implements Attack {
    private final String name;
    private final Map<String, Integer> tribeDamage;
    private final int energyCost;
    private final Tribe tribe;

    AttackImpl(String name, Map<String, Integer> tribeDamage, int energyCost, Tribe tribe) {
        this.name = name;
        this.tribeDamage = tribeDamage;
        this.energyCost = energyCost;
        this.tribe = tribe;
    }

    @Override
    public int getDamage(Tribe tribe) {
        return tribeDamage.get(tribe.name());
    }

    @Override
    public int getEnergyCost() {
        return this.energyCost;
    }

    @Override
    public Tribe getTribe() {
        return this.tribe;
    }

    @Override
    public String getName() {
        return this.name;
    }

    @Override
    public String toString() {
        return "Attack{\n" +
                "name='" + name + '\'' +
                ", \ntribeDamage=" + tribeDamage +
                ", \nenergyCost=" + energyCost +
                ", \ntribe=" + tribe + '\n' +
                '}' + '\n';
    }
}
