package org.radwolfsdragon.game.base;

public interface Attack {
    int getDamage(Tribe tribe);
    int getEnergyCost();
    Tribe getTribe();
    String getName();
}
