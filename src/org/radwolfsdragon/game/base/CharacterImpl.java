package org.radwolfsdragon.game.base;

import java.util.List;

final class CharacterImpl implements Character {
    private final String name;
    private final Tribe tribe;
    private int energy;
    private final List<Attack> attacks;
    private final List<Weapon> weapons;

    CharacterImpl(String name, Tribe tribe, int energy, List<Attack> attacks, List<Weapon> weapons) {
        this.name = name;
        this.tribe = tribe;
        this.energy = this.tribe.getEnergy() + energy;
        this.attacks = attacks;
        this.weapons = weapons;
    }

    @Override
    public void strikeWithWeapon(int weapon, Character victim, World arena) {
        if (weapon > this.weapons.size() - 1) weapon = 0;

        int victimEnergyBefore = victim.getEnergy();
        int myEnergyBefore = this.energy;

        Weapon chosenWeapon = this.weapons.get(weapon);

        // how they are affected
        victim.setEnergy(victimEnergyBefore +
                chosenWeapon.getDamage(Tribe.CHARACTER) +
                arena.getWeaponCharacterDamageEffect() +
                chosenWeapon.getDamage(victim.getTribe()) +
                arena.getEnergyEffect(victim.getTribe())
        );

        // how you are affected
        this.energy = myEnergyBefore + chosenWeapon.getEnergyCost() + arena.getEnergyEffect(this.tribe);

        displayStrikeEffects(String.format("%s used %s on %s.\n%s | %d + %d + %d + %d = %d\n%s | %d + %d + %d = %d",
                this.name,
                chosenWeapon.getName(),
                victim.getName(),
                victim.getName(),
                victimEnergyBefore,
                chosenWeapon.getDamage(Tribe.CHARACTER),
                chosenWeapon.getDamage(victim.getTribe()),
                arena.getEnergyEffect(victim.getTribe()),
                victim.getEnergy(),
                this.name,
                myEnergyBefore,
                chosenWeapon.getEnergyCost(),
                arena.getEnergyEffect(this.tribe),
                this.energy));
    }

    @Override
    public void strikeWithAttack(int attack, Character victim, World arena) {
        if (attack > this.attacks.size() - 1) attack = 0;

        int victimEnergyBefore = victim.getEnergy();
        int myEnergyBefore = this.energy;

        Attack chosenAttack = this.attacks.get(attack);

        // how they are affected
        victim.setEnergy(victimEnergyBefore +
                chosenAttack.getDamage(Tribe.CHARACTER) +
                chosenAttack.getDamage(victim.getTribe()) +
                arena.getEnergyEffect(victim.getTribe())
        );

        // how you are affected
        this.energy = myEnergyBefore + chosenAttack.getEnergyCost() + arena.getEnergyEffect(this.tribe);

        displayStrikeEffects(String.format("%s attacked %s with %s.\n%s | %d + %d + %d + %d = %d\n%s | %d + %d + %d = %d",
                this.name,
                victim.getName(),
                chosenAttack.getName(),
                victim.getName(),
                victimEnergyBefore,
                chosenAttack.getDamage(Tribe.CHARACTER),
                chosenAttack.getDamage(victim.getTribe()),
                arena.getEnergyEffect(victim.getTribe()),
                victim.getEnergy(),
                this.name,
                myEnergyBefore,
                chosenAttack.getEnergyCost(),
                arena.getEnergyEffect(this.tribe),
                this.energy));
    }

    private void displayStrikeEffects(String message) {
        System.out.println(message+'\n');
    }

    @Override
    public int getEnergy() {
        return this.energy;
    }

    @Override
    public void setEnergy(int newEnergy) {
        this.energy = newEnergy;
    }

    @Override
    public Tribe getTribe() {
        return this.tribe;
    }

    @Override
    public String getName() {
        return this.name;
    }

    @Override
    public Attack[] getAttacks() {
        return this.attacks.toArray(new Attack[0]);
    }

    @Override
    public Weapon[] getWeapons() {
        return this.weapons.toArray(new Weapon[0]);
    }

    @Override
    public String toString() {
        return "Character{\n" +
                "name='" + name + '\'' +
                ", \ntribe=" + tribe +
                ", \nenergy=" + energy +
                ", \nattacks=" + attacks +
                ", \nweapons=" + weapons + '\n' +
                '}' + '\n';
    }
}
