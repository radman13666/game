package org.radwolfsdragon.game.base;

public interface Match {
    enum Mode {
        STORY, VERSUS
    }

    Player play();
}
