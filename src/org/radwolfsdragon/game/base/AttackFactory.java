package org.radwolfsdragon.game.base;

import org.radwolfsdragon.game.util.Constants;
import org.radwolfsdragon.game.util.RandomGenerator;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

final class AttackFactory {
    private static final String[] DESCRIPTION = {
            "blast",
            "snare",
            "gust",
            "prayer",
            "curse",
            "blessings",
            "hail",
            "devastation",
            "storm",
            "devourer",
            "bolt",
            "fire",
            "corrosion",
            "chaos"
    };

    private AttackFactory() {}

    static Attack generateRandomAttack(Tribe tribe) {
        int j = RandomGenerator.generateRandomNonNegativeInteger(DESCRIPTION.length);

        return new AttackImpl(DESCRIPTION[j] + " of " + RandomGenerator.generateRandomFiveLetterName(),
                RandomGenerator.generateRandomTribeDamage(),
                -1 * RandomGenerator.generateRandomNonNegativeInteger(Constants.MAX_ATTACK_ENERGY_COST),
                tribe);
    }

    static List<Attack> generateRandomAttacks(int numOfAttacks, Tribe tribe) {
        List<Attack> attacks = new ArrayList<>();
        for (int i = 0; i < numOfAttacks; i++) {
            attacks.add(i, generateRandomAttack(tribe));
        }
        return attacks;
    }

    static Map<String, List<Attack>> generateRandomTribeAttacks() {
        Map<String, List<Attack>> m = new HashMap<>();
        for (Tribe tribe : Constants.TRIBES) {
            if (tribe != Tribe.CHARACTER) {
                m.put(tribe.name(), generateRandomAttacks(Constants.MAX_NUM_OF_ATTACKS_PER_TRIBE, tribe));
            }
        }
        return m;
    }
}
