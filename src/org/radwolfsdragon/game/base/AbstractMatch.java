package org.radwolfsdragon.game.base;

import java.util.List;

abstract class AbstractMatch implements Match {
    protected List<Player> players;
    protected List<Character> creatures;
    protected List<World> arenas;
    protected int numOfBattles;
    protected Battle.Opponent opponent;
    protected List<Battle> battles;

    protected AbstractMatch() {}

    protected AbstractMatch(
            List<Player> players, List<Character> characters,
            List<World> arenas, int numOfBattles, Battle.Opponent opponent) {
        this.players = players;
        this.creatures = characters;
        this.arenas = arenas;
        this.numOfBattles = numOfBattles;
        this.opponent = opponent;
        generateBattles();
    }

    protected void displayWinMessage(String winner) {
        System.out.printf("***> %s Wins! <***\n\n", winner);
    }

    protected abstract void generateBattles();
}
