package org.radwolfsdragon.game.base;

import java.util.UUID;

public final class Player {
    private final String username;
    private final String uuid;
    private int battleWinCounter;

    Player(String username) {
        this.username = username;
        this.uuid = UUID.randomUUID().toString();
        this.battleWinCounter = 0;
    }

    public Fighter makeFighter(Character creature) {
        return new Fighter(this.uuid, creature);
    }

    public void incrementBattleWinCounter() {
        this.battleWinCounter++;
    }

    public String getUsername() {
        return this.username;
    }

    public String getUUID() {
        return this.uuid;
    }

    public int getBattleWinCounter() {
        return this.battleWinCounter;
    }
}
