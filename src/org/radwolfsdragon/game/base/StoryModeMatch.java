package org.radwolfsdragon.game.base;

import org.radwolfsdragon.game.util.Constants;
import org.radwolfsdragon.game.util.RandomGenerator;

import java.util.ArrayList;
import java.util.List;

final class StoryModeMatch extends AbstractMatch {

    private Fighter challengingFighter;

    StoryModeMatch(List<Player> players, List<Character> characters,
                   List<World> arenas, int numOfBattles, Battle.Opponent opponent) {
        super(players, characters, arenas, numOfBattles, opponent);
    }

    @Override
    protected void generateBattles() {
        Character c = this.creatures.remove(RandomGenerator.generateRandomNonNegativeInteger(this.creatures.size()));
        this.challengingFighter = this.players.get(Constants.CHALLENGER).makeFighter(c);

        this.battles = new ArrayList<>();
        for (int j = 0; j < this.numOfBattles; j++) {
            Character d = this.creatures.remove(RandomGenerator.generateRandomNonNegativeInteger(this.creatures.size()));
            Fighter defendingFighter = this.players.get(Constants.DEFENDER).makeFighter(d);

            int w = RandomGenerator.generateRandomNonNegativeInteger(this.arenas.size());
            World arena = this.arenas.remove(w);

            List<Fighter> fighters = new ArrayList<>();
            fighters.add(this.challengingFighter);
            fighters.add(defendingFighter);

            this.battles.add(new BattleImpl(fighters, arena));
        }
    }

    @Override
    public Player play() {
        System.out.println(RandomGenerator.generateRandomBackstory(this.challengingFighter.getFightingCreature())+'\n');

        for (Battle battle : this.battles) {
            Fighter victor = battle.fight(this.opponent, Mode.STORY);
            String playerUUID = victor.getPlayerUUID();
            if (playerUUID.equals(this.players.get(Constants.CHALLENGER).getUUID())) {
                displayWinMessage(String.format("(%s) %s", playerUUID, victor.getFightingCreature().getName()));
                this.players.get(Constants.CHALLENGER).incrementBattleWinCounter();
                incrementChallengingFighterHealth();
            } else {
                displayWinMessage(String.format("(%s) %s", playerUUID, victor.getFightingCreature().getName()));
                return this.players.get(Constants.DEFENDER);
            }
        }
        return this.players.get(Constants.CHALLENGER);
    }

    private void incrementChallengingFighterHealth() {
        int oldEnergy = this.challengingFighter.getFightingCreature().getEnergy();
        oldEnergy += Constants.MAX_STORYMODE_POWERUP;
        this.challengingFighter.getFightingCreature().setEnergy(oldEnergy);
    }
}
