package org.radwolfsdragon.game.base;

public interface Weapon {
    int getDamage(Tribe tribe);
    int getEnergyCost();
    String getName();
}
