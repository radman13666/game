package org.radwolfsdragon.game.base;

public interface World {
    int getEnergyEffect(Tribe tribe);
    int getWeaponCharacterDamageEffect();
    String getName();
}
