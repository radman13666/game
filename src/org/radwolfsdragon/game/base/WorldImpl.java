package org.radwolfsdragon.game.base;

import java.util.Map;

final class WorldImpl implements World {
    private final String name;
    private final int weaponDamageEffect;
    private final Map<String, Integer> tribeEnergyEffect;

    WorldImpl(String name, int weaponDamageEffect, Map<String, Integer> tribeEnergyEffect) {
        this.name = name;
        this.weaponDamageEffect = weaponDamageEffect;
        this.tribeEnergyEffect = tribeEnergyEffect;
    }

    @Override
    public int getEnergyEffect(Tribe tribe) {
        return this.tribeEnergyEffect.get(tribe.name());
    }

    @Override
    public int getWeaponCharacterDamageEffect() {
        return this.weaponDamageEffect;
    }

    @Override
    public String getName() {
        return this.name;
    }

    @Override
    public String toString() {
        return "World{\n" +
                "name='" + name + '\'' +
                ", \nweaponDamageEffect=" + weaponDamageEffect +
                ", \ntribeEnergyEffect=" + tribeEnergyEffect + '\n' +
                '}' + '\n';
    }
}
