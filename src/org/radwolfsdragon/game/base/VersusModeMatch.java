package org.radwolfsdragon.game.base;

import org.radwolfsdragon.game.util.Constants;
import org.radwolfsdragon.game.util.RandomGenerator;

import java.util.ArrayList;
import java.util.List;

final class VersusModeMatch extends AbstractMatch {
    VersusModeMatch(List<Player> players, List<Character> characters,
                    List<World> arenas, int numOfBattles, Battle.Opponent opponent) {
        super(players, characters, arenas, numOfBattles, opponent);
    }

    @Override
    protected void generateBattles() {
        this.battles = new ArrayList<>();
        for (int j = 0; j < this.numOfBattles; j++) {
            Character c = this.creatures.remove(RandomGenerator.generateRandomNonNegativeInteger(this.creatures.size()));
            Fighter challengingFighter = this.players.get(Constants.CHALLENGER).makeFighter(c);

            Character d = this.creatures.remove(RandomGenerator.generateRandomNonNegativeInteger(this.creatures.size()));
            Fighter defendingFighter = this.players.get(Constants.DEFENDER).makeFighter(d);

            int w = RandomGenerator.generateRandomNonNegativeInteger(this.arenas.size());
            World arena = this.arenas.remove(w);

            List<Fighter> fighters = new ArrayList<>();
            fighters.add(challengingFighter);
            fighters.add(defendingFighter);

            this.battles.add(new BattleImpl(fighters, arena));
        }
    }

    @Override
    public Player play() {
        for (Battle battle : this.battles) {
            Fighter victor = battle.fight(this.opponent, Mode.VERSUS);
            String playerUUID = victor.getPlayerUUID();
            if (playerUUID.equals(this.players.get(Constants.CHALLENGER).getUUID())) {
                this.players.get(Constants.CHALLENGER).incrementBattleWinCounter();
            } else {
                this.players.get(Constants.DEFENDER).incrementBattleWinCounter();
            }
            displayWinMessage(String.format("(%s) %s", playerUUID, victor.getFightingCreature().getName()));
        }

        return this.players.get(Constants.CHALLENGER).getBattleWinCounter() >
                this.players.get(Constants.DEFENDER).getBattleWinCounter() ?
                this.players.get(Constants.CHALLENGER) :
                this.players.get(Constants.DEFENDER);
    }
}
