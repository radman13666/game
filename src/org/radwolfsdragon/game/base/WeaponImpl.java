package org.radwolfsdragon.game.base;

import java.util.Map;

final class WeaponImpl implements Weapon {
    private final String name;
    private final int energyCost;
    private final Map<String, Integer> tribeDamage;

    WeaponImpl(String name, Map<String, Integer> tribeDamage, int energyCost) {
        this.name = name;
        this.energyCost = energyCost;
        this.tribeDamage = tribeDamage;
    }

    @Override
    public int getDamage(Tribe tribe) {
        return tribeDamage.get(tribe.name());
    }

    @Override
    public int getEnergyCost() {
        return this.energyCost;
    }

    @Override
    public String getName() {
        return this.name;
    }

    @Override
    public String toString() {
        return "Weapon{\n" +
                "name='" + name + '\'' +
                ", \nenergyCost=" + energyCost +
                ", \ntribeDamage=" + tribeDamage + '\n' +
                '}' + '\n';
    }
}
